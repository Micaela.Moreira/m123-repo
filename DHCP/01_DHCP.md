# DHCP mit Cisco Packet Tracer

## 1. Router Konfiguration auslesen

- Für welches Subnetz ist der DHCP Server aktiv?

    Antwort: 255.255.255.0 mit Range 192.168.26.0/24, wie wir anhand des Befehls "show ip dhcp pool" erkennen.

![router_conifig_01.JPG](DHCP/images/router_config_01.JPG)

- Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)

    | IP Adresse | MAC Adresse |   
    |------------|-------------|
    | 192.168.26.22 | 00:D0:BC:52:B2:9B |
    | 192.168.26.23 | 00:E0:8F:4E:65:AA |
    | 192.168.26.24 | 00:50:0F:4E:1D:82 |
    | 192.168.26.25 | 00:07:EC:B6:45:34 |
    | 192.168.26.26 | 00:01:63:2C:35:08 |
    | 192.168.26.27 | 00:09:7C:A6:4C:E3 |

![router_config _02.JPG](DHCP/images/router_config_02.JPG)

- In welchem Range vergibt der DHCP-Server IPv4 Adressen?

    Antwort: Im Range 192.168.26.0/24, wie wir im vorherigen Bild gesehen haben.

- Was hat die Konfiguration ip dhcp excluded-address zur Folge?

    Antwort: Diese Adresse wird für die statische IP-Vergebung reserviert/rausgenohmen.

- Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?

    Antwort: Im Total hat der DHCP-Server 254 Adressen zur Verfügung, aber nicht alle werden dynamisch vergeben. Es sind, wie im vorherigen Bild erkennbar, 2 Adressen "excluded", welche statisch vergeben werden. Daher kann er 252 dynamisch vergeben.

## DORA - DHCP Lease beobachten

In diesem Schritt soll eine DHCP IPv4-Adress-Zuteilvorgang mitverfolgt werden.

Ziel ist es nun alle vier PDUs zu finden und zu identifzieren.

- Welcher OP-Code hat der DHCP-Discover?

    Antwort: Op Code (op) = 1 (0x1)

    ![OP_Discover.JPG](DHCP/images/OP_Discover.JPG)

- Welcher OP-Code hat der DHCP-Offer?

    Antwort: Op Code (op) = 2 (0x2)

    ![OP_Offer.JPG](DHCP/images/OP_Offer.JPG)

- Welcher OP-Code hat der DHCP-Request?

    Antwort: Op Code (op) = 1 (0x1)

    ![OP_Request.JPG](DHCP/images/OP_Request.JPG)
    
- Welcher OP-Code hat der DHCP-Acknowledge?

    Antwort: Op Code (op) = 2 (0x2)

    ![OP_Acknowledge.JPG](DHCP/images/OP_Acknowledge.JPG)
    
- An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?

    Antwort: An 255.255.255.255. Diese ist die Broadcastadresse um den DHCP-Server zu erreichen.

    ![DST_Discover.JPG](DHCP/images/DST_Discover.JPG)
    
- An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?

    Antwort: An FF:FF:FF:FF:FF:FF (siehe Bild oben). Diese ist die Broadcastadressse für ARP. 
    
- Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?

    Antwort: Weil der Discover per Broadcast gesendet wurde und der Switch das weiterleitet. Der Discover hat keine klare Zieladresse.
    
- Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? Wenn nein, wie kann das mithilfe der DHCP-PDUs festgestellt werden?

    Antwort: Ja der Switch. Er leitet die Kommunikation zwischen DHCP-Server und Endgerät weiter.
    
- Welche IPv4-Adresse wird dem Client zugewiesen?

    Antwort: 192.168.26.28
    
## Netzwerk umkonfigurieren

Aufgabe: Dem Server eine statische IPv4-Adresse zu weisen. Nehmen Sie eine IPv4-Adresse, die sich leicht merken lässt.

![router_config_02.JPG](DHCP/images/router_config_02.JPG)
![Server_0.JPG](DHCP/images/Server_0.JPG)
    