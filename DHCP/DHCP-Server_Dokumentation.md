## Installation Ubuntu Server

Als erstes setzen wir das Linux Betriebssystem auf. Dies lauft wie folgt ab:

 1. "try or install ubuntu server" auswählen.


2. Einstellungen nach Belieben auswählen und durchklicken.

Am Ende sollten wir in der Lage sein uns anzumelden und anzufangen.

![anmeldeseite.JPG](DHCP/images/anmeldeseite.JPG)

## Serverinstallation via Terminal

Alle Befehle können, aber müssten nicht, mit root ausgeführt werden. Ich habe es ohne root ausgeführt.
Dies lauft wie folgt ab:

1. Terminal öffnen und mit folgenden Befehl updaten:

        sudo apt update

    ACHTUNG!!! Da wir dies nicht als root ausführen, werden wir vor dem Ausführen des Befehls das Passwort eingeben.

    ![ubuntuter1.JPG](DHCP/images/ubuntuter1.JPG)

2. DHCP-Dienst mit foldenden Befehl herunterladen:

        sudo apt install isc-dhcp-server -y


    ![ubuntuter2.JPG](DHCP/images/ubuntuter2.JPG)

3. Backup mit folgendem Befehl erstellen:

        sudo cp /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.backup

    (no backup no sorry)

    ![ubuntuter4.JPG](DHCP/images/ubuntuter4.JPG)

4. Konfigurationsdatei mit folgendem Befehl öffnen:

        sudo nano /etc/dhcp/dhcpd.conf

    ![configfile.JPG](DHCP/images/configfile.JPG)

5. Bis zu folgendem Satz runterscrollen:

    ![biszuter.jpg](DHCP/images/biszuter.jpg)

6. Hashtags entfernen und Daten wie gewünscht anpassen.

    ![dhcp_conf_file.JPG](DHCP/images/dhcp_conf_file.JPG)

7. Mit "Ctrl + X" Datei speichern und schliessen.

8. Datei für Netzadapter Konfiguration mit folgendem Befehl öffnen:

        sudo nano /etc/default/isc-dhcp-server

9. "INTERFACEv4" wie gewünscht anpassen.

    ![ubuntuter6.1.JPG](DHCP/images/ubuntuter6.1.JPG)

10. Mit "Ctrl + X" Datei speichern und schliessen.

11. Datei für IP-Adressenfestlegung mit folgendem Befehl öffnen:

        sudo nano /etc/netplan/00-installer-config.yaml

12. Inhalt aus folgendem Bild übernehmen und gegebenfalls anpassen.

    ![dhcp_conf_file2.JPG](DHCP/images/dhcp_conf_file2.JPG)

13. DHCP-Server Rebooten mit folgendem Befehl:

        sudo systemctrl restart isc-dhcp-server

14. Status de DHCP-Servers mit folgendem Befehl  überprüfen:

        sudo systemctrl status isc-dhcp-server

    ![dhcp_failed.JPG](DHCP/images/dhcp_failed.JPG)


## Troubleshooting

Leider hatte meine erste Konfiguration eines DHCP-Servers kein Erfolg, wie man im folgendem Bild sieht. Nach Angaben schliesse ich, dass ich entweder einen Fehler in den Eingaben gemacht habe oder sogar vergessen. 

![genutzt1.JPG](DHCP/images/genutzt1.JPG)
*Quelle:*     https://gitlab.com/ser-cal/m123-lernprodukt/-/blob/main/01_DHCP/README.md

Ich habe anhand einer Git Dokumentation alles Schritt für Schritt durchgeführt. Da nichts fehlte, nehme ich an es liegt entweder an der Ubuntu Version, welche manche andere Schritte oder Angaben voraussetzt oder meine Angaben (IP, Gateway etc.) enthalten Fehler.

Mein nächster Schritt war es anhand von drei verschiedenen Youtube Videos festzustellen, ob ein Schritt vergessen ging, ein Befehl oder eine Angabe fehlte oder sich sonst ein Fehler bemerkbar machte. 

![genutzt2.JPG](DHCP/images/genutzt2.JPG)

*Quellen:*   
    https://www.youtube.com/watch?v=qALgqZvQSM4
    https://www.youtube.com/watch?v=9Vc6-0smd64&t=192s
    https://www.youtube.com/watch?v=H4DZz7p9tug&t=4s


Ich hatte einige Befehle aus den Videos versucht und auch schrittgleich meine Daten überprüft, doch ich konnte keinen Fehler in meinen Angaben feststellen und die zusätzlichen Befehle lösten die Probleme leider nicht. Grund dafür könnte sein, dass ich bereits zu viel versucht hatte und ich einen kompletten Neustart (neue VM und DHCP Installation und Konfiguration) bräuchte um einen eventuellen Erfolg zu erzielen.

Mein nächste Schritt wäre nochmals einen Linux DHCP-Server zu konfigurieren und bei allen Schritten den Status abzufragen, um zu sehen, wann die gleichen Fehlermeldungen auftauchen und bei diesem Schritt in mehreren Quellen nachzuschauen, wo dies auch der Fall war.



