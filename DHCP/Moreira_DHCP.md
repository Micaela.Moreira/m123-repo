# DHCP in a Nutshell

### Dokumentation

Nach dem Verbinden der Geräte, konfiguriere ich als erstes den DHCP-Server. Dazu verwende ich die Infos, welche im Filius File bereits angegeben ist.

![dhcp_setup_infos.png](DHCP/images/dhcp_setup_infos.png)

Mit Doppelklick auf den DCHP-Server überprüfe ich alle Informationen, welche bereits vorhanden sind und passe sie gegeben falls an. 

![dhcp_server_info.JPG](DHCP/images/dhcp_server_info.JPG) 

Danach aktiviere ich «IP forwarding» und gehe weiter zu DHCP Server Setup.

![dhcp_server_setup.JPG](DHCP/images/dhcp_server_setup.JPG)

Ein Fenster mit den «Base settings» wird geöffnet, indem ebenfalls wenn nötig die Informationen entsprechend den Filius Angaben angepasst werden muss. Wichtig ist noch «Activate DHCP» zu aktivieren.

![dhcp_base_settings.JPG](DHCP/images/dhcp_base_settings.JPG) 

Sobald die Informationen stimmen, gehen wir weiter zu «Static Address Assignment» und fügen den Client 3 hinzu, welcher laut Angabe eine statische Adresse bekommt.

![add_static.JPG](DHCP/images/add_static.JPG)

Nach diesen Anpassungen gehen wir zu jedem Client und aktivieren «use DHCP for configuration».

![dhcp_for_config.JPG](DHCP/images/dhcp_for_config.JPG)

Sobald das gemacht ist, kann ich auf den grünen Pfeil drücken und wir sehen sofort, wie die Clients mit dem DHCP-Server kommunizieren, um eine IP zugeteilt zu bekommen. Dies sieht man auf der Datenaustauschanzeige des DHCP-Servers genauer. Dort sehen wir jeden einzelnen Schritt des DORA-Prinzips mit allen Informationen. Dazu gehören die IPs des Absenders und Empfänger, das verwendete Protokoll, der Layer, auf dem die Kommunikation stattfindet, und die Kommentare/Details, die uns genau zeigen, was jeweils passiert ist oder gemacht wurde.

![data_excahnge_dhcp.JPG](DHCP/images/data_excahnge_dhcp.JPG)  
