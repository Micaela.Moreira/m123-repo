# Facts-Sheet "DNS Dienste verstehen"

Lernziele: Wesentliche Funktionen eines DNS-Dienstes kennen, verstehen und erklären

## Einzelauftrag

### Bedeutung, Sinn und Zweck von DNS

Bedeutung: DNS = Domain Name System

Zweck: Der DNS Server löst die IP Adresse nach dem dazugehörigen (menschenfreundlicheren) Domainname auf und vise versa. Diese Informationen hat er in seinem Cache gespeichert. Der DNS Server ist vergleichbar mit einem Telefonbuch fürs Internet.

### Funktionsweise: Ablauf einer DNS-Abfrage

Der DNS Server ist wie ein Telefonbuch für das Internet. Wenn ein Client eine IP Adresse einer Website sucht, sendet er dem DNS Server den Domainname der gewünschten Website (zB. google.ch). Der DNS Server sucht in seinem Cache nach dem Domainname und die zugehörige IP Adresse. Sind die Informationen in seinem Cache, gibt der DNS Server dem Client die gewünschte Information zurück. Wenn die Informationen nicht im Cache sind, sendet er die Anfrage weiter.

### Wichtige Konfigurationsmerkmale

Weiterleitungen:
Weist den DNS-Server an, Anfragen an andere DNS-Server weiterzuleiten, wenn er die Antwort nicht kennt.

Zonen-Einträge: Enthält Informationen über Domains und deren Zuordnung zu IP-Adressen.

Zwischenspeicher:
Speichert bereits beantwortete Anfragen, um zukünftige Anfragen schneller zu beantworten.

Autoritative Einstellungen:
Legt fest, ob der DNS-Server als autoritativ für bestimmte Domains betrachtet wird.

Protokollierung:
Zeichnet Aktivitäten und Anfragen auf, um Probleme zu diagnostizieren und die Leistung zu überwachen.

### Test- und Kontrollkriterien

#### Erreichbarkeit:

Test: Überprüfen, ob der DNS-Server ansprechbar ist.
Kontrolle: Überwachen der Netzwerkkonnektivität zum DNS-Server.

#### Auflösungsgeschwindigkeit:

Test: Messen der Zeit, die der DNS-Server benötigt, um Anfragen zu beantworten.
Kontrolle: Optimierung der Serverressourcen für schnelle Reaktionszeiten.

#### Fehlerbehandlung:

Test: Simulieren von Fehlanfragen und Überprüfen der angemessenen Fehlermeldungen.
Kontrolle: Implementierung von robusten Fehlerbehandlungsmechanismen.

#### Cache-Aktualisierung:

Test: Überprüfen, ob der DNS-Server den Cache regelmäßig aktualisiert.
Kontrolle: Einstellen von Cache-Ablaufzeiten für aktuelle Daten.

#### Sicherheit:

Test: Prüfen auf mögliche Sicherheitslücken und Anfälligkeiten.
Kontrolle: Implementieren von Sicherheitsmaßnahmen wie Zugriffskontrollen und DNSSEC.