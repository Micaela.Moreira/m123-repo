# DNS Dokumentation

### Aufgabenstellung

Aufgabe ist, einen Windows DNS-Server VM aufzusetzen, ihn mit einem Client zu verbinden und mit *ping* und *nslookup* die Funktionsweise zu testen.

Dies wird in den folgenden Texten schrittweise erklärt.

### VMs aufsetzen

Als erstes werden wir sowohl den Client, wie auch den Server aufsetzen. Da bis hier hin die genaue Kenntnis einer VM Erstellung und Vorkonfiguration schon bekannt sein muss und Herr Calisto bereits wunderbare Videos über genau diese Schritte erstellt hat, verzichte ich auf eine detailierte Dokumentation und zeige nur die Unterschiede bei der Server VM auf.

Als erstes wird die Windows Server ISO benötigt, welche jeder IT Schüler der TBZ in Azure Education finden sollte (Link). Diese fügen wir, wie im folgenden Bild, ein.

![server_iso.JPG](DNS/images/server_iso.JPG)

Danach wird die VM gestartet und das Setup beginnt.

Nehme bei der Setup Auswahl *Windows Server 2022 Standart (Desktop Experience) und gehe weiter.

![dns_server-select.JPG](DNS/images/dns_server-select.JPG)

Wähle anschlireesend *Custom: Install Microsoft Server (advanced) und führe die restlichen Konfigurationen nach gewünschten Einstellungen weiter.

![dns_server-select2.JPG](DNS/images/dns_server-select2.JPG)

Sobald die Konfigurationen abgeschlossen sind, sollte folgendes Fenster auftauchen, welches die erfolgreiche Vorkonfiguration beweist.

![dns_server-select_finish.JPG](DNS/images/dns_server-select_finish.JPG)

Installiere als nächstes die VM Tools, wie Herr Calisto in seinen Videos.


### DNS Konfiguration

Installiere die Features für DNS wie folgt:

Gehe beim Server Manager auf *Manage* und wähle *Add Roles and Features*.

![dns_install.JPG](DNS/images/dns_install.JPG)

Gehe im neuen Fenster auf *Installation Type* und wähle *Role-based or features-based installation*.

![dns_install2.JPG](DNS/images/dns_install2.JPG)

Stelle sicher, dass bei *Server Selection* die Option *Select a server from the server pool* activiert ist.

![dns_install4.JPG](DNS/images/dns_install4.JPG)

Wähle bei *Server Roles* die Option *DNS Server* und lasse *File and Storage Services* aktiv.

![dns_install5.JPG](DNS/images/dns_install5.JPG)

Das Fenster *Add Roles and Features Wizard* erscheint, welches alles auflistet, was für die Installation ausgewählt wurde. Stelle sicher, dass alle gewünschten Rollen und oder Features aufgelistet sind und gehe weiter.

![dns_install6.JPG](DNS/images/dns_install6.JPG)

![dns_install7.JPG](DNS/images/dns_install7.JPG)

Bestätige alles und fange die Installation an.

![dns_install8.JPG](DNS/images/dns_install8.JPG)

Warte bis alles installiert wurde.

### Forward-/Reverse-Lookup-Zone

Die Forward- und die Reverse-Lookup-Zone ist zuständig dafür, dass der DNS-Server eine Webseite (zB. google.com) in eine IP Adresse umwandelt und vise versa. 
Wenn aber der lokale DNS (den wir gerade Konfigurieren) die gewünschten Angaben nicht in seinem Cache hat, muss ein Forwarder vorhanden sein, damit er einen anderen DNS-Server im Internet (zB. dns.google.com) rekursiv befragen kann.

Diese drei werden wie folgt eingerichtet:

#### Forward-Lookup-Zone

Gehe im *Server Manager* auf *Tools* und wähle *DNS* aus.

![zonen.JPG](DNS/images/zonen.JPG)

Gehe auf *Win* mit Rechtsklick und wähle *New Zone*.

![zonen2.JPG](DNS/images/zonen2.JPG)

Wähle *Primary zone* und gehe weiter 

![zonen3.png](DNS/images/zonen3.JPG)

Wähle *Forward lookup zone* und gehe weiter.

![zonen4.JPG](DNS/images/zonen4.JPG)

Gebe den gewünschten Zone Name ein (hier: sota.ch) und gehe weiter.

![zonen5.1.JPG](DNS/images/zonen5.1.JPG)

Stelle sicher, dass bei *Create a new file with this file name* der Name stimmt (hier: sota.ch.dns) und gehe weiter.

![zonen6.1.JPG](DNS/images/zonen6.1.JPG)

Forward Lookup Zone wurde erfolgreich eingerichtet.


#### Reverse-Lookup-Zone

Gehe im *Server Manager* auf *Tools* und wähle *DNS* aus.

![zonen.JPG](DNS/images/zonen.JPG)

Gehe auf *Win* mit Rechtsklick und wähle *New Zone*.

![zonen2.JPG](DNS/images/zonen2.JPG)

Wähle *Primary zone* und gehe weiter 

![zonen3.JPG](DNS/images/zonen3.JPG)

Wähle *Reverse lookup zone* und gehe weiter.

![zonen8.JPG](DNS/images/zonen8.JPG)

Wähle *IPv4 Reverse Lookup Zone* und gehe weiter.

![zonen9.JPG](DNS/images/zonen9.JPG)

Gebe die gewünschte Network ID (hier: 192.168.100) und gehe weiter.

![zonen10.JPG](DNS/images/zonen10.JPG)

Prüfe, ob die Angabe stimmt, passe sie gegebenfalls an, und gehe weiter.

![zonen11.JPG](DNS/images/zonen11.JPG)

Reverse Lookup Zone wurde erfolgreich eingerichtet.


#### Records

Nun wird noch ein A-Record erstellt, welcher unsere gewünschten Daten enthält, welche unser DNS authoritiv abfragen kann. Dieser wird wie folgt erstellt:

Gehe mit Rechtsklick auf die erstellte Forward Lookup Zone (hier sota.ch) und wähle *New Host (A or AAAA)*.

![record.JPG](DNS/images/record.JPG)

Gebe den gewünschten Namen und die gewünschte IP ein. Stelle sicher, dass PTR aktiv ist und füge Host hinzu.

Mache das einmal für den DNS Server und einmal für den Client.

![record2.JPG](DNS/images/record2.JPG)

![record3.JPG](DNS/images/record3.JPG)

Prüfe in beiden erstellten Zonen, ob die Angaben drin sind.

![record4.JPG](DNS/images/record4.JPG)

![record5.JPG](DNS/images/record5.JPG)


#### Forwarder

Gehe mit Rechtscklick auf *WIN* Servername und wähle *Properties*.

![record6.JPG](DNS/images/record6.JPG)

Gehe auf *Click here* und gebe dort einen Internet DNS ein (hier: 8.8.8.8).

![record7.JPG](DNS/images/record7.JPG)

dns.google wurde erfolgreich als Forwarder eingericht.

![record8.JPG](DNS/images/record8.JPG)

Hiermit wurden alle Konfigurationen des DNS Servers erfolgreich abgeschlossen.

### Clientteil

Bevor wir zu den Tests kommen, müssen folgende Einstellungen/Anpassungen erfolgt sein.

1. Der DNS Server hat zwei LAN-Schnittstellen,den *NAT* und die Schnittstelle zum Client (hier: M117_LB2).

![LAN_server.JPG](DNS/images/LAN_server.JPG)

2. Der Client hat mindestens die LAN Schnittstelle für die Verbindung zum Server (hier:M117_LB2).

![LAN_client.JPG](DNS/images/LAN_client.JPG)

3. Die Firewall wurde deaktiviert, um das Pingen beim Client zu ermöglichen.

4. Die IPs aus dem A-Record, die wir eingegeben haben, sind entsprechend manuel vergeben.

![Port_server.JPG](DNS/images/Port_server.JPG)

![Port_server2.JPG](DNS/images/Port_server2.JPG)

![port_client.JPG](DNS/images/Port_server3.JPG)

Sobald diese Punkte abgehackt sind, kann getestet werden.

#### Tests

Starte die VMS und melde dich als Admin an (falls mehrer Accounts vorhanden). 

Öffne den CMD auf dem Client und gebe *nslookup* ein. Wenn der DNS-Server angegeben wird, war der Test erfolgreich.

![nslookup_vonclient.JPG](DNS/images/nslookup_vonclient.JPG)

Gebe als nächstes folgendes ein: *nslookup "Server-IP"* (hier: 192.168.100.10). Damit wird getestet, ob der DNS-Server die IP erfolgreich auflösen kann und uns den dazugehörigen Namen gibt. Wenn das der Fall ist, sollte die Angabe aus den Zonen zurückgegeben werden. (hier aus Reverse-lookup-zone)

![nslookup_vonclient2.JPG](DNS/images/nslookup_vonclient2.JPG)

Das gleiche sollte auch im umgekehrten Fall funktionieren. (hier aus Forward-lookup-zone)

![nslookup_vonclient3.JPG](DNS/images/nslookup_vonclient3.JPG)


Jetzt wird der Forwarder getestet. Gebe dazu folgendes ein *nslookup google.com*. Mit diesem Befehl wird getestet, ob der von uns konfigurierter DNS die Anfrage an dns.google weiterleitet, wie im Forwarder festgelegt. Sollte das der Fall sein, wird dns.google die gewünschte Information zurückgeben oder iterativ einholen und zurückgegeben. Das erkennen wir am Kommentar *Non-authorative answer*.

![nslookup_vonclient4.JPG](DNS/images/nslookup_vonclient4.JPG)

Teste zuletzt das Pingen aber auf einen Namen anstelle einer IP. Wenn auch das erfolgreich ist, dann hat der konfigurierte DNS alle tests bestanden.

![ping_dnsgoogle.JPG](DNS/images/ping_dnsgoogle.JPG)
