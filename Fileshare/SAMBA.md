# Samba Share

## Betriebssystem: Ubuntu 22.04

#### System updaten

Update das System, damit es keine Probleme mit der Samba installation gibt.

        sudo apt update

#### Samba Paket installieren

Installiere das Samba Paket

        sudo apt insatll samba

Überprüfe, ob samba läuft.

        systemctl status smbd --no-pager -l

Folgendes eingeben, damit mit dem System automatisch bootet:

        sudo systemctl enable --now smbd

#### Samba in Firewall

Samba Zugriff auf dei Firewall erlauben

        sudo ufw allow samba

#### Samba Gruppe

Füge alle User, welche Zugriff erhalten sollen.

        sudo usermod -aG sambashare "Dein User"

Setze das Passwort für den User.

        sudo smbpasswd -a "dein User"

![smb1.JPG](Fileshare/images/smb1.JPG)

#### Ordner freigeben

Wähle den gewünschten Ordner und geh auf "Properties".

Geh auf "local network share" und aktiviere "share this folder" und "allow others to create and delete files in this folder".

Ein neues Fenster erscheint. Klicke auf "add the permissions automatically".

#### Zugriff auf freigegebenen Ordner

Geh auf den File Manager und clicke auf "other locations".

Geh auf "connect to server" und gebe die Samba IP adresse im folgenden Format:

        smb://"Samba IP"/"Name des freigegebenen Ordners"

![smb2.JPG](Fileshare/images/smb2.JPG)

![smb3.JPG](Fileshare/images/smb3.JPG)

Drücke dann auf connetc.

Login Fenster für den Samba taucht auf. 
Loge als registrierter User ein.

Die ferigegebenen Ordner sollten erscheinen und sowohl zugriffbar wie auch veränderbar sein.

![smb8.JPG](Fileshare/images/smb8.JPG)

![smb9.JPG](Fileshare/images/smb9.JPG)